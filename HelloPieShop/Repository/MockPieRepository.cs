﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloPieShop.Models;

namespace HelloPieShop.Repository
{
    public class MockPieRepository : IPieRepository
    {
        public IEnumerable<Pie> Pies
        {
            get
            {
                return new List<Pie>
                {
                    new Pie{PieId =1, Name="Strawberry Pie",Price=2000,ShortDescription="Lorem Ipsum",LongDescription="Lorem Ipsum",CategoryId = 1, Category = new MockCategoryRepository().Categories.FirstOrDefault(x=>x.CategoryId == 1)},
                    new Pie{PieId=2, Name="Cheese cake",Price=2500,ShortDescription="Lorem Ipsum",LongDescription="Lorem Ipsum", CategoryId = 2, Category = new MockCategoryRepository().Categories.FirstOrDefault(x=>x.CategoryId == 2)},
                    new Pie{PieId=3, Name="Rhubarb Pie",Price=2000,ShortDescription="Lorem Ipsum",LongDescription="Lorem Ipsum", CategoryId = 1, Category = new MockCategoryRepository().Categories.FirstOrDefault(x=>x.CategoryId == 1)},
                    new Pie{PieId=4, Name="Pumpkin Pie",Price=1800,ShortDescription="Lorem Ipsum",LongDescription="Lorem Ipsum", CategoryId = 3, Category = new MockCategoryRepository().Categories.FirstOrDefault(x=>x.CategoryId == 3)}
                };
            }
        }

        public IEnumerable<Pie> PiesOfTheWeek => throw new NotImplementedException();

        public Pie GetPieById(int pieId)
        {
            throw new NotImplementedException();
        }
    }
}
